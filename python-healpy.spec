%global srcname healpy
%global sum Python healpix maps tools

Name:           python-%{srcname}
Version:        1.11.0
Release:        4.1%{?dist}
Summary:        %{sum}

License:        GPLv2+ 
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/h/%{srcname}/%{srcname}-%{version}.tar.gz
Patch0:         %{srcname}-1.10.3-unbundling.patch

# Upstream only supports 64 bit architectures, 32 Bit builds, but tests fail
# and we don't want to provide a non reliable software.
# Check https://github.com/healpy/healpy/issues/194
ExclusiveArch:  aarch64 ppc64 ppc64le x86_64 s390x
# Also explicitly exclude known unsupported architectures
ExcludeArch:    %{ix86} %{arm}

# Common build requirements
BuildRequires:  cfitsio-devel
BuildRequires:  gcc-c++
BuildRequires:  healpix-c++-devel

# Python 3 build requirements
BuildRequires:  python%{python3_pkgversion}-rpm-macros
BuildRequires:  python%{python3_pkgversion}-astropy
BuildRequires:  python%{python3_pkgversion}-Cython
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-matplotlib
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-pytest-runner
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-six

%description
Healpy provides a python package to manipulate healpix maps. It is based on the
standard numeric and visualisation tools for Python, Numpy and matplotlib.

%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{sum}
Requires:       python%{python3_pkgversion}-astropy
Requires:       python%{python3_pkgversion}-matplotlib
Requires:       python%{python3_pkgversion}-six
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

%description -n python%{python3_pkgversion}-%{srcname}
Healpy provides a python package to manipulate healpix maps. It is based on the
standard numeric and visualisation tools for Python, Numpy and matplotlib.

This package contains the Python 3 modules.

%prep
%autosetup -p1 -n %{srcname}-%{version}
# We use system libs for cfitsio and healpix-c++
rm -rf cfitsio healpixsubmodule

%build
%py3_build

%install
%py3_install

%check
pushd %{buildroot}/%{python3_sitearch}
py.test-%{python3_version} healpy
popd

# Note that there is no %%files section for the unversioned python module if we are building for several python runtimes
%files -n python%{python3_pkgversion}-%{srcname}
%license COPYING
%doc CHANGELOG.rst CITATION README.rst
%{python3_sitearch}/*

%changelog
* Wed May 03 2023 Adam Mercer <adam.mercer@ligo.org> - 1.11.0-4.1
- remove python2
- use %{python3_pkgversion} macro for python3 version
- rebuild for el8

* Sat Feb 24 2018 Christian Dersch <lupinix@mailbox.org> - 1.11.0-4
- rebuilt for cfitsio 3.420 (so version bump)

* Wed Feb 14 2018 Christian Dersch <lupinix@mailbox.org> - 1.11.0-3
- rebuilt

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.11.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sun Oct 08 2017 Christian Dersch <lupinix@mailbox.org> - 1.11.0-1
- new version

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.10.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.10.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jun 01 2017 Christian Dersch <lupinix@mailbox.org> - 1.10.3-2
- enable s390x architecture

* Fri Apr 07 2017 Christian Dersch <lupinix@mailbox.org> - 1.10.3-1
- initial package (review: rhbz #1440216)


